<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use \Curl\Curl;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_index")
     */
    public function indexAction()
    {
        $curl = new Curl();
        $curl->get(
            'http://127.0.0.1:8001/ping',
            [
                'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9' .
                    '.eyJvcmdhbml6YXRpb25OYW1lIjoiVW1icmVsbGEyI' .
                    'iwib3JnYW5pemF0aW9uU2l0ZVVSTCI6IjEyMzIuY29' .
                    'tIiwiaWQiOjN9.DOzpKlczfEE7iYRnYiZOPs-XTnge' .
                    '_I_KBYkq-LbtZwA'
            ]);

        if ($curl->error) {
            return new Response('Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n");
        } else {
            return new Response(var_export($curl->response, true));
        }
    }

    /**
     * @Route("/auth", name="auth")
     */
    public function authAction(UserRepository $userRepository)
    {
        $user = $userRepository->find(1);
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
        return new Response('Вы авторизованы');
    }

    /**
     * @Route("/qwe", name="qwe")
     */
    public function qweAction()
    {
        return new Response('1231312');
    }
}
